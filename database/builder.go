package database

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"

	"github.com/ChasingLogic/cardinal/cards"
	"gopkg.in/mgo.v2"
)

// SetupDatabase will init a blank mongodb and make it ready for Cardinal
func SetupDatabase(dialURL string) *mgo.Session {
	fmt.Println("Checking if database needs set up.")
	db := connectToDB(dialURL)
	if checkIfDBBuilt(db.Copy().DB("cardinal")) {
		fmt.Println("Database is initialized booting app.")
		return db
	}

	fmt.Println("Database is uninitialized setting up DB")

	mj := unmarshalMagicCards()
	setupDB(db)

	er := addCardsToDB(db.Copy().DB("cardinal"), mj)
	if er != nil {
		fmt.Println("Error adding cards to database.")
		fmt.Println(er.Error())
		os.Exit(1)
	}

	return db
}

func unmarshalMagicCards() map[string]cards.MagicCard {
	var ujson map[string]cards.MagicCard
	dl, dlerr := http.Get("http://mtgjson.com/json/AllCards-x.json")
	if dlerr != nil {
		fmt.Println("Failed to download from mtgjson.com check your internet connection.")
		fmt.Println(dlerr.Error())
		os.Exit(1)
	}

	contents, err := ioutil.ReadAll(dl.Body)
	if err != nil {
		fmt.Println("Error parsing json file")
		os.Exit(1)
	}

	uerr := json.Unmarshal(contents, &ujson)
	if uerr != nil {
		fmt.Println("Unable to unmarshal json:")
		fmt.Println(uerr.Error())
		os.Exit(1)
	}

	return ujson
}

func connectToDB(dialURL string) *mgo.Session {
	var err error
	var db *mgo.Session

	db, err = mgo.Dial(dialURL)
	if err != nil {
		fmt.Println(err.Error())
		fmt.Println("Failed to connect to mongodb at " + dialURL)
		os.Exit(1)
	}

	return db
}

func checkIfDBBuilt(db *mgo.Database) bool {
	c := db.C("cards")
	cards, err := c.Count()
	if err != nil {
		fmt.Println("Error counting user collection")
		fmt.Println(err.Error())
		os.Exit(1)
	}

	if cards > 0 {
		return true
	}

	return false
}

func addCardsToDB(db *mgo.Database, mm map[string]cards.MagicCard) error {
	cards := db.C("cards")

	for k := range mm {
		err := cards.Insert(mm[k])
		if err != nil {
			return err
		}
	}

	return nil
}

func setupDB(sess *mgo.Session) {
	db := sess.DB("cardinal")

	var ci = mgo.CollectionInfo{}
	ci.DisableIdIndex = false
	ci.ForceIdIndex = true
	ci.Capped = false

	users := db.C("users")
	cards := db.C("cards")
	cardCollections := db.C("cardCollections")
	sso := db.C("sso")

	ue := users.Create(&ci)
	if ue != nil {
		fmt.Println("Error creating users collection")
		fmt.Println(ue.Error())
	}

	ce := cards.Create(&ci)
	if ce != nil {
		fmt.Println("Error creating cards collection")
		fmt.Println(ce.Error())
	}

	cce := cardCollections.Create(&ci)
	if cce != nil {
		fmt.Println("Error creating cardCollections collection")
		fmt.Println(cce.Error())
	}

	sse := sso.Create(&ci)
	if sse != nil {
		fmt.Println("Error creating sso collection")
		fmt.Println(cce.Error())
	}

	fmt.Println("Creating indexes")
	var baseIndex = mgo.Index{
		Key:        []string{"name"},
		Unique:     true,
		Background: false,
		DropDups:   false,
	}

	cards.EnsureIndex(baseIndex)

	cardCollections.EnsureIndex(baseIndex)

	baseIndex.Key = []string{"username"}
	users.EnsureIndex(baseIndex)

	baseIndex.Key = []string{"token"}
	sso.EnsureIndex(baseIndex)

	fmt.Println("Indexes Created")
}
