package handlers

import (
	"net/http"

	"gopkg.in/mgo.v2/bson"

	logger "github.com/Sirupsen/logrus"
	"github.com/ChasingLogic/cardinal/cards"
)

// CardSearch is an HTTP Handler which searches for a card. Takes the game and name of the card from the url
func (m *HandlerManager) CardSearch(w http.ResponseWriter, r *http.Request) {
	db := m.GetADB()

	searchTerm := r.FormValue("cardName")
	game := r.FormValue("game")
	logger.Debug("Searching for " + searchTerm + " in " + game)

	if game == "hearthstone" {
		// Not quite ready for hearthstone
		w.WriteHeader(http.StatusNotImplemented)
	}

	var result []cards.MagicCard
	ferr := db.C(game).Find(bson.M{"name": &bson.M{"$regex": ".*" + searchTerm + ".*", "$options": "i"}}).All(&result)
	if ferr != nil {
		logger.Error(ferr)
		w.WriteHeader(http.StatusNotFound)
	}

	m.RespondWithJSON(w, r, result)
}
