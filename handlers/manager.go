package handlers

import (
	"encoding/json"
	"net/http"

	"github.com/Sirupsen/logrus"
	"gopkg.in/mgo.v2"
)

//HandlerManager is what's going to let us use normal http.Handler's while passing variables around to all of them.
type HandlerManager struct {
	db      string
	session *mgo.Session
	logger  *logrus.Logger
}

// GetADB copies the mgo session and returns a database instance
func (m *HandlerManager) GetADB() *mgo.Database {
	return m.session.Copy().DB(m.db)
}

// RespondWithJSON attempts to marshal the object then sends the resulting json back to the client
// unless there is an error in which case it 500's.
func (m *HandlerManager) RespondWithJSON(w http.ResponseWriter, r *http.Request, obj interface{}) {
	mo, merr := json.Marshal(obj)
	if merr != nil {
		m.logger.Error(merr)
		w.WriteHeader(http.StatusInternalServerError)
	}

	w.Write(mo)
}

// CloseSession closes the mongodb session
func (m *HandlerManager) CloseSession() {
	m.session.Close()
}

// NewHandlerManager creates a new HandlerManager intializing the appropriate values.
func NewHandlerManager(sess *mgo.Session, dbname string, logger *logrus.Logger) *HandlerManager {
	var m HandlerManager
	m.session = sess
	m.logger = logger
	m.db = dbname
	return &m
}
