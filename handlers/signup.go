package handlers

import (
	"encoding/json"
	"net/http"
)

// SignupHandler accepts a json formatted user then will add the defaults to the user and will update them into the given collection.
// It then returns this new user to the front-end
func (m *HandlerManager) SignupHandler(w http.ResponseWriter, r *http.Request) {
	users := m.GetADB().C("users")

	decoder := json.NewDecoder(r.Body)
	var u User
	m.logger.Debug("signup handler called")

	err := decoder.Decode(&u)
	if err != nil {
		m.logger.Error("Decoding error: " + err.Error())
		w.WriteHeader(http.StatusInternalServerError)
	}

	if u.Username == "" {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("No username provided."))
	}

	u.Password = hasher.Sum(u.Password)
	u.DashItems = append(u.DashItems, DashItem{
		Img:     "/img/defaultDashItem.jpg",
		Title:   "Dash Item Title",
		Content: "This is your default dash item! You can create your own by choosing \"Edit Dash\" from the side Menu!",
	})

	derr := users.Insert(&u)
	if derr != nil {
		m.logger.Error("Insertion error: " + derr.Error())
		w.WriteHeader(http.StatusForbidden)
		w.Write([]byte("User already exists"))
	}

	marshaledU, merr := json.Marshal(u)
	if merr != nil {
		m.logger.Error(merr.Error())
		w.WriteHeader(http.StatusInternalServerError)
	}

	m.logger.Info("User " + u.Username + " has successfully signed up.")
	w.Write(marshaledU)
}
