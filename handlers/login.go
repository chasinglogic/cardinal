package handlers

import (
	"crypto/sha256"
	"encoding/json"
	"net/http"

	logger "github.com/Sirupsen/logrus"
	"gopkg.in/mgo.v2/bson"
)

var (
	hasher  = sha256.New()
	keySize = 32
)

// DashItem is the basic form of a "Dash Card" which displays various information to the user.
type DashItem struct {
	Img     string `json:"img"`
	Title   string `json:"title"`
	Content string `json:"content"`
}

// User is to hold the user information in our DB
type User struct {
	ID          bson.ObjectId `json:"id" bson:"_id,omitempty"`
	Username    string        `json:"username"`
	Password    []byte        `json:"-"`
	DashItems   []DashItem    `json:"dashitems"`
	Collections []interface{} `json:"collections"`
}

// LoginHandler will log a user in if they exist otherwise will return an error.
// We just send the full user as a json object instead of splitting it across a url param and json object as is the norm.
func (m *HandlerManager) LoginHandler(w http.ResponseWriter, r *http.Request) {
	db := m.GetADB()

	decoder := json.NewDecoder(r.Body)
	var u User
	var udb User
	m.logger.Debug("login handler called")

	err := decoder.Decode(&u)
	if err != nil {
		logger.Error(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
	}

	derr := db.C("users").Find(bson.M{"username": u.Username}).One(&udb)
	if derr != nil {
		logger.Error(derr.Error())
		w.WriteHeader(http.StatusForbidden)
	}

	u.Password = hasher.Sum(u.Password)
	if string(u.Password) == string(udb.Password) {
		token, err := generateToken(db, u.Username)
		if err != nil {
			logger.Error(err.Error())
			w.WriteHeader(http.StatusInternalServerError)
		}

		logger.Info("User " + u.Username + " has successfully logged in.")
		logger.Debug("Token generated for " + u.Username + " is " + token)
		w.Header().Set("AuthToken", token)
		m.RespondWithJSON(w, r, udb)
	} else {
		logger.Info("User " + u.Username + " failed login attempt.")
		w.WriteHeader(http.StatusForbidden)
	}
}
