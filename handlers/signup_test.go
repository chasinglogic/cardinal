package handlers

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestSignupHandler(t *testing.T) {
	mU, mE := json.Marshal(User{Username: "testSignup", Password: []byte("test")})
	if mE != nil {
		t.Fatal("Marshal Error")
	}

	req, _ := http.NewRequest("POST", "/user/signup", bytes.NewReader(mU))
	res := httptest.NewRecorder()

	ghm.SignupHandler(res, req)

	if res.Code != 200 {
		FailAndLog(t, http.StatusOK, res.Code, res.Body)
	}

	// This make sure that a user can't signup twice or with someone elses username
	sreq, _ := http.NewRequest("POST", "/user/signup", bytes.NewReader(mU))
	sres := httptest.NewRecorder()

	ghm.SignupHandler(sres, sreq)

	if sres.Code != http.StatusForbidden {
		FailAndLog(t, http.StatusForbidden, sres.Code, sres.Body)
	}
}
