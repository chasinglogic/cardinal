package handlers

import (
	"crypto/rand"
	"encoding/base64"
	"time"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// UserToken respresents an SSO token for a given user.
type UserToken struct {
	Username string
	Token    string
	Created  time.Time
}

func generateToken(db *mgo.Database, un string) (string, error) {
	var rs string

	for {
		buffer := make([]byte, keySize)
		_, err := rand.Read(buffer)
		if err != nil {
			return "", err
		}

		rs = base64.URLEncoding.EncodeToString(buffer)

		derr := db.C("sso").Insert(UserToken{
			Token:    rs,
			Username: un,
			Created:  time.Now(),
		})

		if derr == nil {
			return rs, nil
		}
	}
}

func validateToken(db *mgo.Database, t string) bool {
	var ut UserToken

	sso := db.C("sso")
	fe := sso.Find(bson.M{"token": t})
	if x, _ := fe.Count(); x != 1 {
		return false
	}

	fe.One(&ut)
	if time.Since(ut.Created).Minutes() > 30 {
		db.C("sso").Remove(bson.M{"token": t})
		return false
	}

	return true
}
