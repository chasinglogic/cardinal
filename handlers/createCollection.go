package handlers

import (
	"encoding/json"
	"net/http"

	"github.com/ChasingLogic/cardinal/cards"
	logger "github.com/Sirupsen/logrus"
)

// CreateCollection will add a collection to the appropriate user
func (m *HandlerManager) CreateCollection(w http.ResponseWriter, r *http.Request) {
	logger.Info("Creating collection.")
	db := m.GetADB()

	decoder := json.NewDecoder(r.Body)
	var mc cards.Collection

	err := decoder.Decode(&mc)
	if err != nil {
		logger.Error(err.Error())
		w.WriteHeader(500)
	}

	uerr := db.C("cardCollections").Insert(mc)
	if uerr != nil {
		logger.Error(err.Error())
		w.WriteHeader(500)
	}

	w.WriteHeader(200)
}
