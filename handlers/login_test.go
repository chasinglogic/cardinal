package handlers

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/ChasingLogic/cardinal/database"
	"github.com/Sirupsen/logrus"
)

var ghm *HandlerManager

func init() {
	log := logrus.New()
	log.Out = os.Stdout
	log.Level = logrus.DebugLevel
	session := database.SetupDatabase("localhost:27017")
	ghm = NewHandlerManager(session, "cardinal", log)
}

func FailAndLog(t *testing.T, expected int, actual int, body *bytes.Buffer) {
	t.Logf("Body: %v\n", body.String())
	t.Fatalf("Expected: %v Got: %v", expected, actual)
}

func TestLoginHandler(t *testing.T) {
	mU, mE := json.Marshal(User{Username: "testLogin", Password: []byte("test")})
	if mE != nil {
		t.Fatal("Marshal Error")
	}

	req, _ := http.NewRequest("GET", "/user/login/", bytes.NewReader(mU))
	res := httptest.NewRecorder()

	ghm.LoginHandler(res, req)

	if res.Code != http.StatusForbidden {
		FailAndLog(t, http.StatusForbidden, res.Code, res.Body)
	}

	sureq, _ := http.NewRequest("GET", "/user/login", bytes.NewReader(mU))
	sures := httptest.NewRecorder()

	ghm.SignupHandler(sures, sureq)

	lreq, _ := http.NewRequest("GET", "/user/login", bytes.NewReader(mU))
	lres := httptest.NewRecorder()

	ghm.LoginHandler(lres, lreq)

	if lres.Code != 200 {
		FailAndLog(t, http.StatusOK, lres.Code, lres.Body)
	}
}

func TestGenerateToken(t *testing.T) {
	firstToken, fe := generateToken(ghm.GetADB(), "test")
	if fe != nil {
		t.Fatal("Read error on first token: " + fe.Error())
	}

	secondToken, se := generateToken(ghm.GetADB(), "test2")
	if se != nil {
		t.Fatal("Read error on second token: " + se.Error())
	}

	t.Log("Tokens generated")

	if firstToken == secondToken {
		t.Fatalf("Expected tokens to not match instead firstToken: %s, secondToken %s", firstToken, secondToken)
	}
}

func TestValidateToken(t *testing.T) {
	testToken, _ := generateToken(ghm.GetADB(), "validUser")
	valid := validateToken(ghm.GetADB(), testToken)

	if !valid {
		t.Fatal("Token was not valid. Expected to be valid.")
	}
}
