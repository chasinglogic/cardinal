package handlers

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/ChasingLogic/cardinal/cards"
)

func TestCreateCollection(t *testing.T) {
	// Need to make sure a user is created first.
	mU, mE := json.Marshal(User{Username: "testCreateCollection", Password: []byte("test")})
	if mE != nil {
		t.Fatal("Marshal Error")
	}

	req, _ := http.NewRequest("POST", "/user/signup", bytes.NewReader(mU))
	res := httptest.NewRecorder()

	ghm.SignupHandler(res, req)

	sreq, _ := http.NewRequest("GET", "/user/login/", bytes.NewReader(mU))
	sres := httptest.NewRecorder()

	ghm.LoginHandler(sres, sreq)

	var user User

	lerr := json.Unmarshal(sres.Body.Bytes(), &user)
	if lerr != nil {
		t.Fatal("login error")
	}

	mMC, err := json.Marshal(cards.Collection{
		Owner:  user.ID,
		Name:   "testColl",
		IsMain: false,
		Cards:  []cards.CardStub{},
	})

	if err != nil {
		t.Fatal("Marshal Error: " + err.Error())
	}

	req, _ = http.NewRequest("GET", "/api/v1/createCollection?user=test", bytes.NewReader(mMC))
	res = httptest.NewRecorder()

	ghm.CreateCollection(res, req)

	if res.Code != 200 {
		t.Fatalf("Expected: %v: Got: %v", "200", res.Code)
	}
}
