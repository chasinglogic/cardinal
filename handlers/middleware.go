package handlers

import (
	"net/http"
	"time"
)

// StopWatch is a logging middleware which will log the route, method, and time spent of a request.
func (m *HandlerManager) StopWatch(h http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()
		h.ServeHTTP(w, r)
		stop := time.Now()
		m.logger.Infof("[%s] %q %v\n", r.Method, r.URL.String(), stop.Sub(start))
	}

	return http.HandlerFunc(fn)
}

// DontPanicBro will keep our app from falling over in the case of a goroutine error.
func (m *HandlerManager) DontPanicBro(h http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if err := recover(); err != nil {
				m.logger.Error(err)
				http.Error(w, http.StatusText(500), 500)
			}
		}()

		h.ServeHTTP(w, r)
	}

	return http.HandlerFunc(fn)
}

// AuthMe will verify that a user's auth token is valid.
func (m *HandlerManager) AuthMe(h http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		token := r.Header.Get("Authorization")
		if validateToken(m.GetADB(), token) {
			h.ServeHTTP(w, r)
		} else {
			http.Redirect(w, r, "/login", 401)
		}
	}

	return http.HandlerFunc(fn)
}
