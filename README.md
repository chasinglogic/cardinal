# Cardinal [![Build Status](https://travis-ci.org/ChasingLogic/cardinal.svg)](https://travis-ci.org/ChasingLogic/cardinal) [![Gitter](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/ChasingLogic/cardinal?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=body_badge)
This app has been a dream of mine for a long time. I'm a huge fan of card games of all types and always wanted an app that I could throw my card collections into and run reports on.


### How To Help
We're still in the early days of development so grab an issue, make a branch and start hacking!

Make sure that in your pull requests and commit messages you list any issue #'s that you are fixing/resolving.

### What you'll need

You'll need [node](http://nodejs.org/) to update your card database (This is being fixed currently see issue #8)

You'll need [Go](http://golang.org) installed to work on the back-end. You can install all Go dependencies using go get in the server folder.

### Looking for Front-End Work?

Currently I'm only working on the API and will write a front-end next using Ember.js if you want to start that project from the ground up you can head over to [cardinal-frontend](http://github.com/ChasingLogic/cardinal-frontend)

**NOTE:** What I accept in that repo will be subject to my opinions and my own ability to understand the code which my front-end especially with Ember.js is really weak at this point since that's a learning project for me. Try to be gentle with your comments and we'll be cool. The other option is to write your own front-end for the Cardinal API which is part of the reason I designed the app this way.

### Suggestions?
I love them. Good, bad, or ugly I love feedback and suggestions you can open a github issue if appropriate or you can send me an email or [tweet.](http://twitter.com/ChasingLogic)
