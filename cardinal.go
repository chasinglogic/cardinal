package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"reflect"
	"strings"

	"github.com/ChasingLogic/cardinal/database"
	"github.com/ChasingLogic/cardinal/handlers"
	"github.com/Sirupsen/logrus"
	"github.com/gorilla/mux"
	"github.com/tylerb/graceful"
	"gopkg.in/mgo.v2"

	viper "github.com/spf13/viper"
)

var (
	session *mgo.Session
	db      *mgo.Database
	logger  *logrus.Logger
)

func loggerInit() (*os.File, logrus.Level) {
	var logFile *os.File
	var fileErr error
	filename := "cardinal.out"

	if _, err := os.Stat(filename); err == nil {
		logFile, fileErr = os.OpenFile(filename, os.O_RDWR|os.O_APPEND, 0660)
		logFile.WriteString("\n")
		if fileErr != nil {
			fmt.Println(fileErr.Error())
			os.Exit(1)
		}

	} else {
		_, pErr := os.Stat("../logs/")

		if os.IsNotExist(pErr) {
			os.Mkdir("../logs/", 0777)
		}

		logFile, fileErr = os.Create(filename)
		if fileErr != nil {
			fmt.Println(fileErr.Error())
			os.Exit(1)
		}
	}

	switch strings.ToLower(viper.GetString("server.loglevel")) {

	case "debug":
		return logFile, logrus.DebugLevel

	case "info":
		return logFile, logrus.InfoLevel

	case "warn":
		return logFile, logrus.WarnLevel

	case "error":
		return logFile, logrus.ErrorLevel

	case "fatal":
		return logFile, logrus.FatalLevel

	case "panic":
		return logFile, logrus.PanicLevel

	default:
		return logFile, logrus.InfoLevel

	}
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "client/views/index.html")
}

func notFoundHandler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Looks like you went on a journey to nowhere."))
}

func main() {

	viper.AddConfigPath("./")
	viper.SetConfigFile("config.toml")
	viper.ReadInConfig()

	logger = logrus.New()

	logFile, logLevel := loggerInit()
	mwriter := io.MultiWriter(os.Stdout, logFile)

	logger.Out = mwriter
	logger.Level = logLevel
	logger.Formatter = &logrus.TextFormatter{
		FullTimestamp:   true,
		TimestampFormat: "01/02/2006 15:04:05",
	}

	logger.Info("Cardinal starting")
	logger.Info("Log file: " + logFile.Name())
	logger.Info("=========Settings===========")
	for k, v := range viper.AllSettings() {
		if reflect.TypeOf(v).Kind() == reflect.Map {
			logger.Infof("%s", k)
			for sk, sv := range viper.GetStringMapString(k) {
				logger.Infof("\t%s = %s", sk, sv)
			}
		} else {
			logger.Infof("%s = %s", k, v)
		}

	}
	logger.Info("============================")
	var dialURL string

	if viper.GetString("database.user") == "" {
		dialURL = "mongodb://" + viper.GetString("database.ip") + ":" + viper.GetString("database.port") + "/" + viper.GetString("database.name")
	} else {
		dialURL = "mongodb://" + viper.GetString("database.user") + ":" + viper.GetString("database.password") + "@" +
			viper.GetString("database.ip") + ":" + viper.GetString("database.port") + "/" + viper.GetString("database.name")
	}
	sess := database.SetupDatabase(dialURL)

	logger.Info("Building HandlerManager")
	var HandlerManager = handlers.NewHandlerManager(sess, viper.GetString("database.name"), logger)

	logger.Info("mongodb connection successful")

	router := mux.NewRouter()

	router.HandleFunc("/api/v1/userAuth/login", HandlerManager.LoginHandler).Methods("POST")
	router.HandleFunc("/api/v1/userAuth/signup", HandlerManager.SignupHandler).Methods("POST")
	router.HandleFunc("/api/v1/createCollection", HandlerManager.CreateCollection).Methods("POST")
	router.HandleFunc("/api/v1/cardSearch", HandlerManager.CardSearch).Methods("GET")

	logger.Info("Serving client files, I recommend using NGINX instead.")
	router.HandleFunc("/", indexHandler)
	router.PathPrefix("/").Handler(http.FileServer(http.Dir("client/")))

	logger.Info("Server ready on port " + viper.GetString("server.port"))

	graceful.ListenAndServe(&http.Server{
		Addr:    viper.GetString("server.ip") + ":" + viper.GetString("server.port"),
		Handler: router,
	}, 0)
}
