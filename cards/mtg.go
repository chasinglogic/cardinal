package cards

// MagicCard represents a Magic: The Gathering card and implements the Card Interface
type MagicCard struct {
	ID           string              `json:"id"`
	Layout       string              `json:"layout"`
	Name         string              `json:"name"`
	ManaCost     string              `json:"manaCost"`
	Cmc          float32             `json:"cmc"`
	Colors       []string            `json:"colors"`
	Type         string              `json:"type"`
	Types        []string            `json:"types"`
	SubTypes     []string            `json:"subTypes"`
	Power        string              `json:"power"`
	Toughness    string              `json:"toughness"`
	Text         string              `json:"text"`
	ForeignNames []map[string]string `json:"foreignNames"`
	Printings    []string            `json:"printings"`
	Legalities   []map[string]string `json:"legalities"`
	ImageNames   []string            `json:"imageNames"`
}

// GetID implements the Card Interface
func (m MagicCard) GetID() string {
	return m.ID
}

// GetName implements the Card Interface
func (m MagicCard) GetName() string {
	return m.Name
}
