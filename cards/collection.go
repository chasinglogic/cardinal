package cards

import "gopkg.in/mgo.v2/bson"

// CardStub allows us to store all kinds of cards regardless of game in a Collection's Cards array
type CardStub struct {
	ID   bson.ObjectId `json:"id" bson:"_id,omitempty"`
	Name string
}

// Collection is a collection of cards. It has an owner, name, game, slice of cards, and whether it is the main collection or not.
type Collection struct {
	ID     bson.ObjectId `json:"id" bson:"_id,omitempty"`
	Owner  bson.ObjectId `json:"owner" bson:"owner"`
	Name   string        `json:"name"`
	Game   string        `json:"game"`
	IsMain bool          `json:"ismain"`
	Cards  []CardStub    `json:"cards"`
}
